# ExecSQl-in-Java


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;





public class ExecSQL{

   public static void main(String[] args) throws Exception{
     
     if (args.length == 0)
     {
       System.out.println("Usage: java ExecSQL propertiesFile" + "[statementFile]");
       System.exit(0);
     }   
     
     SimpleDataSource.init(args[0]);
     
     Connection conn = SimpleDataSource.getConnection();
     Statement stat = conn.createStatement();
     
     Reader reader;
     if (args.length > 1)
          reader = new FileReader(args[1]);
      else
        reader = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(reader);
        
        boolean done = false;
        while(!done)
        {
          String line = in.readLine();
          if (line == null)
           done = true;
         else
         {
            try
            {
                boolean hasResultSet = stat.execute(line);
                if (hasResultSet)
                  showResultSet(stat);
             }
             catch (SQLException exception)
             {
                exception.printStackTrace();
             }       
         }
      }   
        
           in.close();
           stat.close();
           conn.close();
        }
        
        public static void showResultSet(Statement stat)throws SQLException {
		 
		 ResultSet result = stat.getResultSet();
		 ResultSetMetaData metaData = result.getMetaData();
		 int columnCount = metaData.getColumnCount();
		 
		 for (int i = 1; i <= columnCount; i++)
		 {
		    if (i > 1) System.out.print(", ");
		    System.out.println(metaData.getColumnLabel(i));	 
	     }	
           System.out.println();
         
         while (result.next())
         {
			for (int i = 1; i <= columnCount; i++)
			{
			  if (i > 1) System.out.print(",");
			  System.out.println(result.getString(i));	
			} 
			System.out.println();
           }
           result.close();
         }
       }    
           
      
   
  
